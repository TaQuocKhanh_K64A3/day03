<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    .container {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 34vh;
  margin-top: 2rem;
}

.form-group {
  width: 30vw;
  height: inherit;
  background-color: white;
  padding: 2rem;
  margin-top: 2rem;
  display: flex;
  flex-direction: column;
  border: 2px solid #4f85b4;
}

.form-child {
  height: 30px;
  margin: 10px 0px;
}

.form-child {
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin: 1.4rem 0px;
}

.sub-form-child {
  background-color: #5a9bd5;
  height: inherit;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px 6px;
  border: 2px solid #4f85b4;
  color: white;
}

.form-text {
  height: inherit;
  width: 180px;
  padding: 0px;
  border: 2px solid #4f85b4;
}

.form-radio {
  display: flex;
  align-items: center;
  height: inherit;
  width: 180px;
  padding: 0px;
}

.form-input-select {
  display: flex;
  align-items: center;
  height: inherit;
  width: 180px;
  padding: 0px;
}

.form-child-btn {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 2rem;
}

.btn-submit {
  height: 40px;
  width: 150px;
  border-radius: 5px;
  border: 2px solid #4f85b4;
  background-color: #70ad46;
  color: white;
}

  </style>
  <title>Register</title>
</head>

<body>
  <div class="container">
    <div class="form-group">
      <form >
        <div class="form-child">
          <label class="sub-form-child">Họ và tên</label>
          <input type="text" name="name" class="form-text" >
        </div>

        <div class="form-child">
          <label class="sub-form-child">Giới tính</label>
          <div class="form-radio">
            <?php
            $gioiTinh = array('0' => 'Nam', '1' => 'Nữ');
            for ($i = 0; $i < count($gioiTinh); $i++) {
              echo '
                <input type="radio" id="' . $i . '" name="gender" value="' . $i . '">
                ';
              echo '
                <label for="' . $i . '" style="margin-right: 10px">' . $gioiTinh[$i] . '</label> 
                ';
            }
            ?>
          </div>
        </div>

        <div class="form-child">
          <label class="sub-form-child">Phân khoa</label>
          <div class="form-input-select">
            <select id="hello" style="height: inherit">
              <?php
              $khoa = array('null' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
              foreach ( $khoa as $key => $value) {
                echo '<option value="' . $key . '">' . $value . '</option>';
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-child-btn">
          <input type="submit" name="btn" class="btn-submit" value="Đăng ký">
        </div>
      </form>
    </div>
  </div>
</body>

</html>
